package favorites

import (
	catalogClient "gitlab.com/o-cloud/catalog/api/public"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Favorite struct {
	ID                primitive.ObjectID         `json:"-" bson:"_id,omitempty"`
	Name              string                     `json:"name" bson:"name,omitempty"`
	ProviderID        string                     `json:"providerId" bson:"providerId"`
	ProviderType      catalogClient.ProviderType `json:"providerType" bson:"providerType"`
	Description       string                     `json:"description" bson:"description,omitempty"`
	OriginClusterName string                     `json:"originClusterName" bson:"originClusterName,omitempty"`
	EndpointLocation  string                     `json:"endpointLocation" bson:"endpointLocation,omitempty"`
	WorkflowStructure string                     `json:"workflowStructure" bson:"workflowStructure,omitempty"`
	IsLocal           bool                       `json:"isLocal" bson:"isLocal"`
	Metadata          map[string]interface{}     `json:"metadata" bson:"metadata"`
}
