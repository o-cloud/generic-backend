package favorites

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	catalogClient "gitlab.com/o-cloud/catalog/api/public"
	"gitlab.com/o-cloud/cwl"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type FavoriteService struct {
	Service IService
}

func NewHandler() *FavoriteService {
	return &FavoriteService{
		Service: NewService(),
	}
}

func (h *FavoriteService) SetupRoutes(g *gin.RouterGroup) {
	g.GET("/", h.getFavorites)
	g.POST("/", h.postFavorite)
	g.DELETE("/:providerId", h.deleteFavorite)
}

type getFavoriteResponse struct {
	ID                primitive.ObjectID         `json:"-" bson:"_id,omitempty"`
	IsLocal           bool                       `json:"isLocal" bson:"isLocal"`
	Name              string                     `json:"name" bson:"name,omitempty"`
	ProviderID        string                     `json:"providerId" bson:"providerId"`
	ProviderType      catalogClient.ProviderType `json:"providerType" bson:"providerType"`
	Description       string                     `json:"description" bson:"description,omitempty"`
	OriginClusterName string                     `json:"originClusterName" bson:"originClusterName,omitempty"`
	EndpointLocation  string                     `json:"endpointLocation" bson:"endpointLocation,omitempty"`
	WorkflowStructure cwl.CommandLineTool        `json:"workflowStructure" bson:"workflowStructure,omitempty"`
	Metadata          map[string]interface{}     `json:"metadata" bson:"metadata"`
}

func parseTool(str string) (*cwl.CommandLineTool, error) {
	if isBase64(str) {
		b, err := base64.StdEncoding.DecodeString(str)
		if err != nil {
			return nil, err
		}
		return cwl.ParseTool(bytes.NewReader(b))
	} else {
		return cwl.ParseTool(strings.NewReader(str))
	}
}

func isBase64(str string) bool {
	r := regexp.MustCompile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")
	return r.MatchString(str)
}

func (h *FavoriteService) getFavorites(ctx *gin.Context) {
	res, err := h.Service.GetAllFavorites()
	if err != nil {
		fmt.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	tab := make([]getFavoriteResponse, len(res))

	for i, fav := range res {
		tool := &cwl.CommandLineTool{}
		if fav.ProviderType != catalogClient.Computing {
			tool, err = parseTool(fav.WorkflowStructure)
			if err != nil {
				fmt.Printf("error while parsing tool %s, cwl: %s\n", fav.Name, fav.WorkflowStructure)
				fmt.Println(err)
				ctx.Status(http.StatusInternalServerError)
				return
			}
		}

		tab[i] = getFavoriteResponse{
			ID:                fav.ID,
			Name:              fav.Name,
			ProviderID:        fav.ProviderID,
			ProviderType:      fav.ProviderType,
			Description:       fav.Description,
			OriginClusterName: fav.OriginClusterName,
			EndpointLocation:  fav.EndpointLocation,
			IsLocal:           fav.IsLocal,
			WorkflowStructure: *tool,
			Metadata:          fav.Metadata,
		}
	}
	ctx.JSON(http.StatusOK, tab)
}

func (h *FavoriteService) postFavorite(ctx *gin.Context) {
	var favorite Favorite
	err := ctx.BindJSON(&favorite)
	if err != nil {
		ctx.Error(err)
		ctx.Status(http.StatusBadRequest)
		return
	}
	err = h.Service.InsertFavorite(&favorite)
	if err != nil {
		ctx.Error(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}

	if favorite.IsLocal == false {
		// err = h.retrieveFavoriteRates(favorite.OriginClusterName, favorite.ProviderID, string(favorite.ProviderType))
		err = h.retrieveFavoriteRates(favorite.OriginClusterName, favorite.Name, string(favorite.ProviderType))
		if err != nil {
			fmt.Print("Error retrieving favorite rates: ", err)
			ctx.Error(err)
			ctx.Status(http.StatusInternalServerError)
		}
	}

	ctx.JSON(http.StatusCreated, favorite)
}

func (h *FavoriteService) retrieveFavoriteRates(originClusterName string, providerID string, providerType string) error {
	// Retrieve cost for new favorite
	var path string
	if providerType == "computing" {
		path = fmt.Sprintf("%s/api/pricing-manager/rates/computing/%s", viper.GetString("PRICING_MANAGER_API"), originClusterName)
	} else {
		path = fmt.Sprintf("%s/api/pricing-manager/rates/provider/%s/%s", viper.GetString("PRICING_MANAGER_API"), originClusterName, providerID)
	}
	_, err := http.Get(path)
	if err != nil {
		fmt.Println("Error while contacting local pricing-manager: ", err)
	}
	return err
}

func (h *FavoriteService) deleteFavorite(ctx *gin.Context) {
	providerId := ctx.Param("providerId")
	if len(providerId) == 0 {
		ctx.Error(fmt.Errorf("no providerId in the request"))
		ctx.Status(http.StatusBadRequest)
		return
	}
	err := h.Service.DeleteFavorite(providerId)
	if err != nil {
		ctx.Error(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusOK)
}
