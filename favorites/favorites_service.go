package favorites

import (
	"context"
	"fmt"
	"time"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func getFavoritesCollection() *mongo.Collection {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))
	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}
	return client.Database("genericBackend").Collection("favorites")
}

type IService interface {
	GetAllFavorites() ([]Favorite, error)
	InsertFavorite(favorite *Favorite) error
	DeleteFavorite(providerId string) error
	IsFavorite(providerId string) bool
}

type mongoCollection struct {
	col *mongo.Collection
}

func NewService() IService {
	return &mongoCollection{
		col: getFavoritesCollection(),
	}
}

func (s *mongoCollection) GetAllFavorites() ([]Favorite, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	results := make([]Favorite, 0)
	cur, err := s.col.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	cur.All(ctx, &results)
	return results, err
}

func (s *mongoCollection) InsertFavorite(f *Favorite) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	update := bson.D{{"$set", *f}}
	filter := bson.D{{"providerId", f.ProviderID}}
	upsert := options.Update().SetUpsert(true)
	_, err := s.col.UpdateOne(ctx, filter, update, upsert)
	if err != nil {
		return err
	}
	return nil
}

func (s *mongoCollection) DeleteFavorite(providerId string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.D{{"providerId", providerId}}

	result, err := s.col.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	fmt.Println("nombre d'élements supprimés", result.DeletedCount)

	return nil
}

func (s *mongoCollection) IsFavorite(providerId string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.D{{"providerId", providerId}}
	count, err := s.col.CountDocuments(ctx, filter, options.Count().SetLimit(1))
	if err != nil {
		fmt.Println(err)
		return false
	} else {
		return count == 1
	}
}
