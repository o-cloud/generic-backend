package providers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type ProviderService struct {
	Service IService
}

func NewHandler() *ProviderService {
	return &ProviderService{
		Service: NewService(),
	}
}

func (h *ProviderService) SetupRoutes(g *gin.RouterGroup) {
	g.GET("/", h.getProviders)
}

type ProviderForResponse struct {
	Name              string             `json:"name,omitempty"`
	Description       string             `json:"description,omitempty"`
	Version           string             `json:"version,omitempty"`
	Type              string             `json:"type,omitempty"`
	Images            []string           `json:"images,omitempty"`
	CatalogRegistered int                `json:"catalogSyncStatus"`
	CreatedAt         *time.Time         `json:"createdAt,omitempty"`
	UpdatedAt         *time.Time         `json:"updatedAt,omitempty"`
	RegisteredAt      *time.Time         `json:"registeredAt,omitempty"`
	UnregisteredAt    *time.Time         `json:"unregisteredAt,omitempty"`
}

type ProviderResponse struct {
	Providers []ProviderForResponse `json:"providers" binding:"required"`
}

func (h *ProviderService) getProviders(ctx *gin.Context) {
	res, err := h.Service.GetAllProviders()
	if err != nil {
		fmt.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	tab := make([]ProviderForResponse, len(res))

	for i, pro := range res {
		registeredAt := &pro.RegisteredAt
		if registeredAt.IsZero() {
			registeredAt = nil
		}
		unregisteredAt := &pro.UnregisteredAt
		if unregisteredAt.IsZero() {
			unregisteredAt = nil
		}
		tab[i] = ProviderForResponse{
			Name:              pro.Name,
			Description:       pro.Description,
			Version:           pro.Version,
			Type:              pro.Type,
			Images:            pro.Images,
			CatalogRegistered: pro.CatalogRegistered,
			CreatedAt:         &pro.CreatedAt,
			UpdatedAt:         &pro.UpdatedAt,
			RegisteredAt:      registeredAt,
			UnregisteredAt:    unregisteredAt,
		}

	}
	ctx.JSON(http.StatusOK, ProviderResponse{
		Providers: tab,
	})
}
