package providers

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Provider struct {
	Id                primitive.ObjectID `bson:"_id,omitempty"`
	Name              string             `bson:"name,omitempty"`
	Description       string             `bson:"description,omitempty"`
	Version           string             `bson:"version,omitempty"`
	Type              string             `bson:"type,omitempty"`
	Images            []string           `bson:"images,omitempty"`
	CatalogRegistered int                `bson:"catalog_sync_status"`
	CreatedAt         time.Time          `bson:"created_at,omitempty"`
	UpdatedAt         time.Time          `bson:"updated_at,omitempty"`
	RegisteredAt      time.Time          `bson:"registered_at,omitempty"`
	UnregisteredAt    time.Time          `bson:"unregistered_at,omitempty"`
}
