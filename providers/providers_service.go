package providers

import (
	"context"
	"time"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func getProvidersCollection() *mongo.Collection {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))
	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}
	return client.Database("providers").Collection("providers")
}

type IService interface {
	GetAllProviders() ([]Provider, error)
}

type mongoCollection struct {
	col *mongo.Collection
}

func NewService() IService {
	return &mongoCollection{
		col: getProvidersCollection(),
	}
}

func (s *mongoCollection) GetAllProviders() ([]Provider, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	results := make([]Provider, 0)
	cur, err := s.col.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	cur.All(ctx, &results)
	return results, err
}