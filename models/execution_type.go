package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Execution struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Namespace    string             `json:"namespace" bson:"namespace"`
	ArgoName     string             `json:"argoName" bson:"argoName"`
	WorkflowName string             `json:"workflowName" bson:"workflowName"`
	Status       string             `json:"status" bson:"status"`
	WorkflowID   primitive.ObjectID `json:"workflowId" bson:"workflowId"`
	StartedAt    string             `json:"startedAt" bson:"startedAt"`
	FinishedAt   string             `json:"finishedAt" bson:"finishedAt"`
	Tasks        []Task             `json:"tasks" bson:"tasks"`
}

type ExecutionFilters struct {
	WorkflowID *primitive.ObjectID
	StartTime  *time.Time
}

type Task struct {
	Name            string              `json:"name" bson:"name"`
	BlockID         int                 `json:"blockId" bson:"blockId"`
	Status          string              `json:"status" bson:"status"`
	StartedAt       string              `json:"startedAt" bson:"startedAt"`
	FinishedAt      string              `json:"finishedAt" bson:"finishedAt"`
	NodeStatus      map[string]string   `json:"nodeStatus" bson:"nodeStatus"`
	Logs            map[string][]string `json:"logs" bson:"logs"`
	ComputeProvider ComputerProvider    `json:"computeProvider" bson:"computeProvider"`
	CostLimit       float32             `json:"costLimit" bson:"costLimit,omitempty"`
	PodName         string              `json:"podName" bson:"podName"`
}

type ExecutionStatusCount struct {
	Succeeded int64 `json:"succeeded"`
	Failed    int64 `json:"failed"`
	Running   int64 `json:"running"`
}
