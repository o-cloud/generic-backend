package models

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Workflow struct {
	Id        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name      string             `json:"name" bson:"name,omitempty"`
	User      string             `json:"user" bson:"user,omitempty"`
	Blocks    []Block            `json:"blocks" bson:"blocks"`
	Links     []Link             `json:"links" bson:"links"`
	RunConfig RunConfigJSON      `json:"runConfig" bson:"runConfig,omitempty"`
	// Maybe we can improve this next structure...
	MetricResults map[string]map[string]MetricPrometheusResponseStruct `json:"metricsByType" bson:"metricsByType"`
	//MetricResults map[string]RemoteMetricResults `json:"metricsByType" bson:"metricsByType"`
	RemoteMetricsRates map[string]map[string]float64 `json:"remoteMetricsRates" bson:"remoteMetricsRates"`
}

//type RemoteMetricResults struct {
//	//	MetricType          string                                    `json:"metricType" bson:"metricType"`
//	RemoteClusterMetric map[string]MetricPrometheusResponseStruct `json:"remoteClusterMetric" bson:"remoteClusterMetric"`
//}

type MetricPrometheusResponseStruct struct {
	Status string `json:"status"`
	Data   struct {
		ResultType string `json:"resultType"`
		Result     []struct {
			Metric struct {
				Pod string `json:"pod"`
			} `json:"metric"`
			Values [][]interface{} `json:"values"`
		} `json:"result"`
	} `json:"data"`
}

type Block struct {
	Index       int           `json:"index" bson:"index"`
	InputPorts  []interface{} `json:"inputPorts" bson:"inputPorts"`
	OutputPorts []interface{} `json:"outputPorts" bson:"outputPorts"`
	Name        string        `json:"name" bson:"name"`
	X           float32       `json:"x" bson:"x"`
	Y           float32       `json:"y" bson:"y"`
	Payload     struct {
		Cwl              interface{}            `json:"cwl" bson:"cwl"`
		EndpointLocation string                 `json:"endpointLocation" bson:"endpointLocation"`
		ID               int                    `json:"id" bson:"id"`
		Kind             string                 `json:"kind" bson:"kind"`
		Parameters       map[string]interface{} `json:"parameters" bson:"parameters"`
		ProviderType     string                 `json:"providerType" bson:"providerType"`
		ComputeProvider  ComputerProvider       `json:"computeProvider" bson:"computeProvider"`
		S3Provider       ComputerProvider       `json:"s3Provider" bson:"s3Provider"`
		CostLimitBlock   float32                `json:"costLimitBlock" bson:"costLimitBlock"`
		ClusterName      string                 `json:"clusterName" bson:"clusterName"`
	} `json:"payload" bson:"payload"`
}

type Link struct {
	ToIndex       int    `json:"toIndex" bson:"toIndex"`
	PortFromIndex int    `json:"portFromIndex" bson:"portFromIndex"`
	PortToIndex   int    `json:"portToIndex" bson:"portToIndex"`
	FromIndex     int    `json:"fromIndex" bson:"fromIndex"`
	FromName      string `json:"fromName" bson:"fromName"`
	ToName        string `json:"toName" bson:"toName"`
}

type ComputerProvider struct {
	ProviderId        string      `json:"providerId" bson:"_id,omitempty"`
	ProviderType      string      `json:"providerType" bson:"providerType,omitempty"`
	Description       string      `json:"description"  bson:"description,omitempty"`
	OriginClusterName string      `json:"originClusterName"  bson:"originClusterName,omitempty"`
	EndpointLocation  string      `json:"endpointLocation"  bson:"endpointLocation,omitempty"`
	Name              string      `json:"name"  bson:"name,omitempty"`
	WorkflowStructure interface{} `json:"workflowStructure"  bson:"workflowStructure,omitempty"`
	IsLocal           bool        `json:"isLocal"  bson:"isLocal,omitempty"`
	Metadata          interface{} `json:"metadata" bson:"metadata"`
	CostLimitCluster  float32     `json:"costLimitCluster" bson:"costLimitCluster,omitempty"`
}

type RunConfigJSON struct {
	ComputerProvider ComputerProvider `json:"computeProvider"  bson:"computeProvider"`
}

// ReindexBlocks : modify the index of the blocks ensuring that for each block in the workflow wf.Block[i].Index == i
func (wf *Workflow) ReindexBlocks() {
	oldIndexToNewIndex := make(map[int]int, len(wf.Blocks))
	for i, block := range wf.Blocks {
		oldIndexToNewIndex[block.Index] = i
		wf.Blocks[i].Index = i
	}
	for i := range wf.Links {
		wf.Links[i].FromIndex = oldIndexToNewIndex[wf.Links[i].FromIndex]
		wf.Links[i].ToIndex = oldIndexToNewIndex[wf.Links[i].ToIndex]
	}
}

// SortBlocks : Topological sort of the blocks list
func (wf *Workflow) SortBlocks() error {
	blocksByIndex := make(map[int]Block)
	childrenMap := make(map[int][]int, len(wf.Blocks))
	inDegrees := make(map[int]int, len(wf.Blocks))
	sorted := make([]Block, 0, len(wf.Blocks))
	queue := make([]Block, 0)
	for _, block := range wf.Blocks {
		childrenMap[block.Index] = make([]int, 0)
		inDegrees[block.Index] = 0
		blocksByIndex[block.Index] = block
	}
	for _, link := range wf.Links {
		childrenMap[link.FromIndex] = append(childrenMap[link.FromIndex], link.ToIndex)
		inDegrees[link.ToIndex]++
	}
	for index, degree := range inDegrees {
		if degree == 0 {
			queue = append(queue, blocksByIndex[index])
		}
	}
	for len(queue) > 0 {
		e := queue[0]
		queue = queue[1:]
		sorted = append(sorted, e)
		for _, childIndex := range childrenMap[e.Index] {
			inDegrees[childIndex]--
			if inDegrees[childIndex] == 0 {
				queue = append(queue, blocksByIndex[childIndex])
			}
		}
	}
	for _, inDegree := range inDegrees {
		if inDegree > 0 {
			return fmt.Errorf("error while sorting wf, wf is invalid")
		}
	}
	wf.Blocks = sorted
	return nil
}
