package catalog

import (
	"context"
	"fmt"
	"log"
	"strings"
	"sync"

	"github.com/gorilla/websocket"
	catalogClient "gitlab.com/o-cloud/catalog/api/public"
	discovery_api "gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/generic-backend/config"
	"gitlab.com/o-cloud/generic-backend/favorites"
)

type service interface {
	find(providerType catalogClient.ProviderType, id string, url string) (catalogClient.ProviderInfo, error)
}

func newService() service {
	return &serviceImpl{}
}

type serviceImpl struct {
}

func (s *serviceImpl) find(providerType catalogClient.ProviderType, id string, url string) (catalogClient.ProviderInfo, error) {
	client := catalogClient.NewClient(url)
	result, err := client.GetProviderInfo(context.TODO(), providerType, id)
	if err != nil {
		return catalogClient.ProviderInfo{}, err
	}
	return result, nil
}

type WsSearchService struct {
	socket          *websocket.Conn
	favoriteService favorites.IService
	peersUrl        []PeerMap
	mu              sync.Mutex
}

type PeerMap struct {
	catalogUri   string
	endpointHost string
	peer         discovery_api.Peer
	isLocal      bool
}

func (service *WsSearchService) init() error {
	api := discovery_api.NewClusterDiscoveryApiClient(config.Config.PeersDiscoveryUri)
	fmt.Println(api)
	peers, err := api.ListPeers()
	if err == nil {
		service.peersUrl = make([]PeerMap, len(peers.Peers))
		for index, peer := range peers.Peers {
			service.peersUrl[index].catalogUri = peer.URL + config.Config.PublicCatalogPrefix
			service.peersUrl[index].endpointHost = peer.URL
			service.peersUrl[index].peer = peer
			service.peersUrl[index].isLocal = false
		}
	} else {
		log.Printf("unable to get peers %s \n", err)
	}
	//ajout du local
	// FIXME: Local Peer ID is empty, so local request will appear as anonymous.
	// in any case, we should send a proove of our identity and not a simple text
	identities, err := api.ListIdentities()
	if err != nil {
		log.Printf("error while listing identities, %s", err)
	}
	log.Printf("identities : %#v", identities)

	if len(identities.Identity.Name) > 0 {
		iden := identities.Identity
		service.peersUrl = append(service.peersUrl, PeerMap{config.Config.CatalogUri, "", discovery_api.Peer{Name: iden.Name, URL: iden.URL, Description: iden.Description}, true})
	} else {
		service.peersUrl = append(service.peersUrl, PeerMap{config.Config.CatalogUri, "", discovery_api.Peer{Name: "OfflineLocalCluster"}, true})
	}
	return nil
}

func (service *WsSearchService) Listen() {
	defer service.socket.Close()
	for {
		var query SearchQuery
		//TODO Message générique
		err := service.socket.ReadJSON(&query)
		if err != nil {
			log.Println(err)
			break
		}
		//TODO switch sur le message
		err = service.HandleSearch(query)
		if err != nil {
			log.Println(err)
			break
		}
	}
}

func (service *WsSearchService) HandleSearch(query SearchQuery) error {

	nbOfClusters := len(service.peersUrl)
	fmt.Println("nbOfClusters", nbOfClusters)
	for i := 0; i < nbOfClusters; i++ {
		peerUrl := service.peersUrl[i]
		go func() {
			result, err := search(query.ProviderType, query.Terms, peerUrl.catalogUri, peerUrl)
			if err != nil {
				fmt.Println(fmt.Errorf("error while searching in catalog %s, %s", peerUrl.catalogUri, err))
				return
			}
			responses := service.createSearchResponse(result, peerUrl)
			service.mu.Lock()
			defer service.mu.Unlock()
			if err = service.socket.WriteJSON(responses); err != nil {
				log.Println(err)
			}
		}()
	}
	return nil
}

func search(providerType catalogClient.ProviderType, terms, url string, originPeer PeerMap) ([]catalogClient.ProviderInfo, error) {
	client := catalogClient.NewClient(url)

	var peerID string
	if originPeer.isLocal {
		peerID = "local"
	} else {
		peerID = originPeer.peer.Name
	}

	result, err := client.Search(context.TODO(), providerType, terms, peerID)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (service *WsSearchService) createSearchResponse(in []catalogClient.ProviderInfo, peerMap PeerMap) []searchResponse {
	ret := make([]searchResponse, len(in))
	for i, v := range in {

		searchResponse := searchResponse{
			ProviderInfo:      v,
			IsFavorite:        service.favoriteService.IsFavorite(v.ProviderId),
			OriginClusterName: peerMap.peer.Name,
			IsLocal:           peerMap.isLocal,
		}
		searchResponse.EndpointLocation = strings.ReplaceAll(searchResponse.EndpointLocation, "{{CLUSTER_HOST}}", peerMap.peer.URL)
		ret[i] = searchResponse
	}
	return ret
}
