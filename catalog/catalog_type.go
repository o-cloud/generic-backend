package catalog

import (
	catalogClient "gitlab.com/o-cloud/catalog/api/public"
)

type searchResponse struct {
	catalogClient.ProviderInfo
	IsFavorite        bool   `json:"isFavorite"`
	IsLocal           bool   `json:"isLocal"`
	OriginClusterName string `json:"originClusterName"`
}
