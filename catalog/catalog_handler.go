package catalog

import (
	"bytes"
	"encoding/base64"
	"log"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/o-cloud/cwl"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	catalogClient "gitlab.com/o-cloud/catalog/api/public"
	"gitlab.com/o-cloud/generic-backend/config"
	"gitlab.com/o-cloud/generic-backend/favorites"
)

type Handler struct {
	service         service
	favoriteService favorites.IService
}

func NewHandler() *Handler {
	return &Handler{
		service:         newService(),
		favoriteService: favorites.NewService(),
	}
}

func (h *Handler) SetUpRoutes(g *gin.RouterGroup) {
	g.GET("/:providerType/search", h.Search)
	g.GET("/:providerType/find/:id", h.Find)
}

type SearchQuery struct {
	Terms        string                     `json:"terms"`
	ProviderType catalogClient.ProviderType `json:"providerType"`
}

func (h *Handler) Search(ctx *gin.Context) {

	upgrader := websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool {
		//TODO check if we need to be more strict
		return true
	}

	ws, err := upgrader.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		log.Println(err)
		return
	}
	service := WsSearchService{socket: ws, favoriteService: h.favoriteService}
	service.init()
	service.Listen()
}

func (h *Handler) Find(ctx *gin.Context) {
	providerType := catalogClient.ProviderType(ctx.Param("providerType"))
	id := ctx.Param("id")
	url := config.Config.CatalogUri
	result, err := h.service.find(providerType, id, url)
	if err != nil {
		ctx.Error(err)
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}
	tool, err := parseTool(result.WorkflowStructure)
	if err != nil {
		ctx.Error(err)
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, GetProviderResponse{
		Id:                result.Id,
		ProviderId:        result.ProviderId,
		Type:              result.Type,
		Version:           result.Version,
		Name:              result.Name,
		Description:       result.Description,
		Metadata:          result.Metadata,
		EndpointLocation:  result.EndpointLocation,
		WorkflowStructure: *tool,
	})
}

func parseTool(str string) (*cwl.CommandLineTool, error) {
	if isBase64(str) {
		b, err := base64.StdEncoding.DecodeString(str)
		if err != nil {
			return nil, err
		}
		return cwl.ParseTool(bytes.NewReader(b))
	} else {
		return cwl.ParseTool(strings.NewReader(str))
	}
}

func isBase64(str string) bool {
	r := regexp.MustCompile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")
	return r.MatchString(str)
}

type GetProviderResponse struct {
	Id                string                     `json:"id"`
	ProviderId        string                     `json:"providerId"`
	Type              catalogClient.ProviderType `json:"providerType"`
	Version           string                     `json:"version"`
	Name              string                     `json:"name"`
	Description       string                     `json:"description"`
	Metadata          map[string]interface{}     `json:"metadata"`
	EndpointLocation  string                     `json:"endpointLocation"`
	WorkflowStructure cwl.CommandLineTool        `json:"workflowStructure"`
}
