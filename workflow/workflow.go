package workflow

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/o-cloud/generic-backend/models"

	mongo_helper "gitlab.com/o-cloud/generic-backend/mongo-helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Service struct {
	col *mongo_helper.Collection
}

func GetWorkflowService() *Service {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//TODO maybe we don't need this at the end. It allows reading interface{} in the db without having an array of key/values as output
	rb := bson.NewRegistryBuilder()
	rb.RegisterTypeMapEntry(bsontype.EmbeddedDocument, reflect.TypeOf(bson.M{}))

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")).SetRegistry(rb.Build()))
	//client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://root:nLKqZxPO0U@localhost:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false").SetRegistry(rb.Build()))

	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}
	c := mongo_helper.NewCollection(client.Database("genericBackend").Collection("workflow"))
	return &Service{
		col: c,
	}
}

func (w *Service) insert(document models.Workflow) (*models.Workflow, error) {
	res, errInsert := w.col.InsertOne(document)
	if errInsert != nil {
		log.Fatal(errInsert)
	}
	document.Id = res
	return &document, errInsert
}

func (w *Service) FindOne(id string) (models.Workflow, error) {
	var result models.Workflow
	err := w.col.FindOneById(id, &result)
	if err == mongo.ErrNoDocuments {
		fmt.Println("record does not exist")
	}
	return result, err
}

func (w *Service) findAll() ([]models.Workflow, error) {
	results := make([]models.Workflow, 0)
	err := w.col.FindAll(&results)
	log.Print("resultMongo", results)
	return results, err
}

func (w *Service) deleteOne(id string) error {
	err := w.col.DeleteOneById(id)
	return err
}

func (w *Service) updateOne(id string, workflow models.Workflow) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = w.col.Col.ReplaceOne(ctx, bson.M{"_id": objectID}, workflow)
	return err
}

func (w *Service) findWorkflowUser(user string) ([]models.Workflow, error) {
	var result []models.Workflow
	err := w.FindByUser(user, &result)
	if err == mongo.ErrNoDocuments {
		fmt.Println("record does not exist")
	}
	return result, err
}
func (w *Service) FindByUser(user string, result interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cur, err := w.col.Col.Find(ctx, bson.M{"user": user})
	if err != nil {
		log.Fatal(err)
		return err
	}
	defer cur.Close(ctx)
	cur.All(ctx, result)
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	return err
}
