package workflow

import (
	"gitlab.com/o-cloud/generic-backend/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AllWorkflowResponseContainer struct {
	Workflows []AllWorkflowsResponseItem `json:"workflows"`
}

type AllWorkflowsResponseItem struct {
	Id         primitive.ObjectID     `json:"id"`
	Name       string                 `json:"name"`
	User       string                 `json:"user"`
	Executions []ExecutionWithoutLogs `json:"executions"`
}

func (_ AllWorkflowsResponseItem) fromWorkflow(workflow models.Workflow, exec []models.Execution) AllWorkflowsResponseItem {
	r := AllWorkflowsResponseItem{
		Id:         workflow.Id,
		Name:       workflow.Name,
		User:       workflow.User,
		Executions: make([]ExecutionWithoutLogs, len(exec)),
	}
	for i, execution := range exec {
		r.Executions[i] = ExecutionWithoutLogs{}.fromExecution(execution)
	}
	return r
}

type ExecutionWithoutLogs struct {
	ID           primitive.ObjectID `json:"id"`
	Namespace    string             `json:"namespace"`
	ArgoName     string             `json:"argoName"`
	WorkflowName string             `json:"workflowName"`
	Status       string             `json:"status"`
	WorkflowID   primitive.ObjectID `json:"workflowId"`
	StartedAt    string             `json:"startedAt"`
	FinishedAt   string             `json:"finishedAt"`
	Tasks        []TaskWithoutLogs  `json:"tasks"`
}

func (_ ExecutionWithoutLogs) fromExecution(exec models.Execution) ExecutionWithoutLogs {
	result := ExecutionWithoutLogs{
		ID:           exec.ID,
		Namespace:    exec.Namespace,
		ArgoName:     exec.ArgoName,
		WorkflowName: exec.WorkflowName,
		Status:       exec.Status,
		WorkflowID:   exec.WorkflowID,
		StartedAt:    exec.StartedAt,
		FinishedAt:   exec.FinishedAt,
		Tasks:        make([]TaskWithoutLogs, len(exec.Tasks)),
	}

	for i, task := range exec.Tasks {
		result.Tasks[i] = TaskWithoutLogs{}.fromTask(task)
	}

	return result
}

type TaskWithoutLogs struct {
	Name       string  `json:"name"`
	BlockID    int     `json:"blockId"`
	Status     string  `json:"status"`
	StartedAt  string  `json:"startedAt"`
	FinishedAt string  `json:"finishedAt"`
	CostLimit  float32 `json:"costLimit"`
	PodName    string  `json:"podName"`
}

func (_ TaskWithoutLogs) fromTask(task models.Task) TaskWithoutLogs {
	result := TaskWithoutLogs{
		Name:       task.Name,
		BlockID:    task.BlockID,
		Status:     task.Status,
		StartedAt:  task.StartedAt,
		FinishedAt: task.FinishedAt,
		CostLimit:  task.CostLimit,
		PodName:    task.PodName,
	}
	return result
}
