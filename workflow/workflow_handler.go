package workflow

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/generic-backend/executions"
	"gitlab.com/o-cloud/generic-backend/models"
)

type Handler struct {
	Service          *Service
	ExecutionService executions.Service
}

func NewHandler() *Handler {
	return &Handler{
		Service:          GetWorkflowService(),
		ExecutionService: executions.NewService(),
	}
}

func (w *Handler) SetUpRoutes(router *gin.RouterGroup) {
	router.GET("", w.GetAllWorkflowsHandler)
	router.GET("/:id", w.getOneWorkflowHandler)
	router.GET("/users", w.getUserWorkflowHandler)
	router.POST("", w.createWorkflowHandler)
	router.POST("/:id", w.updateWorkflowHandler)
	router.DELETE("/:id", w.deleteWorkflowHandler)
	router.POST("/:id/start", w.executeWorkflow)
}

type FoundWorkFlow struct {
	Id      string   `json:"id"`
	Name    string   `json:"name"`
	Cluster []string `json:"cluster"`
}

func (w *Handler) getUserWorkflowHandler(c *gin.Context) {
	user := c.Query("user")
	var r []models.Workflow
	var err error
	if len(user) > 0 {
		r, err = w.Service.findWorkflowUser(user)
	} else {
		r, err = w.Service.findAll()
	}

	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusNotFound)
		return
	}
	var response []FoundWorkFlow
	for _, temp := range r {
		var found = FoundWorkFlow{
			Id:      temp.Id.Hex(),
			Name:    temp.Name,
			Cluster: extractCluster(temp),
		}
		response = append(response, found)
	}
	c.JSON(http.StatusOK, response)
}

func extractCluster(temp models.Workflow) []string {
	var clusters []string
	var stringLocation []string
	for _, block := range temp.Blocks {
		if &block.Payload != nil {
			if &block.Payload.EndpointLocation != nil {
				stringLocation = strings.Split(block.Payload.EndpointLocation, "/api")
			}
		}
		clusters = append(clusters, stringLocation[0])
	}
	return clusters
}

func (w *Handler) GetAllWorkflowsHandler(c *gin.Context) {
	workflows, err := w.Service.findAll()
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	res := make([]AllWorkflowsResponseItem, len(workflows))
	for i := range workflows {
		exec, err := w.ExecutionService.FindAll(models.ExecutionFilters{
			WorkflowID: &workflows[i].Id,
		})
		if err != nil {
			_ = c.Error(err)
			c.Status(http.StatusInternalServerError)
			return
		}
		res[i] = AllWorkflowsResponseItem{}.fromWorkflow(workflows[i], exec)
	}
	c.JSON(http.StatusOK, AllWorkflowResponseContainer{Workflows: res})
}

func (w *Handler) getOneWorkflowHandler(c *gin.Context) {
	id := c.Param("id")
	r, err := w.Service.FindOne(id)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusNotFound)
		return
	}
	c.JSON(http.StatusOK, r)
}

func (w *Handler) createWorkflowHandler(c *gin.Context) {
	var workflow models.Workflow
	if err := c.BindJSON(&workflow); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	res, err := w.Service.insert(workflow)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, res)
}

func (w *Handler) updateWorkflowHandler(c *gin.Context) {
	var workflow models.Workflow
	if err := c.BindJSON(&workflow); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusUnprocessableEntity)
		return
	}
	id := c.Param("id")
	err := w.Service.updateOne(id, workflow)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, workflow)
}

func (w *Handler) executeWorkflow(c *gin.Context) {
	var workflow models.Workflow
	if err := c.BindJSON(&workflow); err != nil {
		_ = c.Error(err)
		c.Status(http.StatusUnprocessableEntity)
		return
	}
	id := c.Param("id")
	err := w.Service.updateOne(id, workflow)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	wf, err := w.Service.FindOne(id)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusNotFound)
		return
	}
	exec, err := w.ExecutionService.ExecuteWorkflow(wf)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, exec)
}

func (w *Handler) deleteWorkflowHandler(c *gin.Context) {
	id := c.Param("id")
	err := w.Service.deleteOne(id)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusNotFound)
		return
	}
	c.Status(http.StatusOK)
}
