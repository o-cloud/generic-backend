package prometheushandler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/generic-backend/executions"
	"gitlab.com/o-cloud/generic-backend/models"
	"gitlab.com/o-cloud/generic-backend/workflow"
)

func (handler *PrometheusHandler) PrometheusAPI(request *gin.RouterGroup) {
	request.GET("/cluster", handler.GetLocalClusterMetrics)
	request.GET("/cluster/remote/metrics", handler.GetWorkflowRemoteMetric)
	request.GET("/cluster/remote/cost-by-metric", handler.GetWorkflowRemoteCostAverageByMetric)
	request.GET("/cluster/remote/cost-all-metrics", handler.GetWorkflowRemoteCostAverageAllMetrics)
	request.GET("/cluster/remote/metrics-rate", handler.ReturnMetricsRateToAPICall)
}

func (handler *PrometheusHandler) GetLocalClusterMetrics(context *gin.Context) {
	param := url.Values{}
	addParam(&param, context)
	querySearch := getQueryByMetric(param.Get("metric"))
	queryToSend := fmt.Sprintf(querySearch, param.Get("cluster"), param.Get("namespace"))
	paramEncode := strings.ReplaceAll(param.Encode(), ",", "&")
	print("params: ", paramEncode)
	var path string

	if isMissing := isMissingParamsInURL(context); isMissing {
		return
	}

	// Set path to call local prometheus
	path = fmt.Sprintf(QueryByTime, "http://prometheus-kube-prometheus-prometheus.monitoring.svc.cluster.local:9090", queryToSend, paramEncode)
	response, err := http.Get(path)
	if err != nil {
		context.JSON(http.StatusUnprocessableEntity, "fail to contact prometheus")
		context.Error(err)
		return
	}

	// Store the http response in an dedicated struct
	prometheusResponseMetric := models.MetricPrometheusResponseStruct{}
	json.NewDecoder(response.Body).Decode(&prometheusResponseMetric)
	context.JSON(http.StatusOK, prometheusResponseMetric)
}

func GetMetricList() []string {
	metricList := make([]string, 0)
	metricList = append(metricList, "cpu_usage")
	metricList = append(metricList, "cpu_request")
	metricList = append(metricList, "memory_usage")
	metricList = append(metricList, "network_transmitted")
	metricList = append(metricList, "network_received")

	return metricList
}

func getQueryByMetric(metric string) string {
	var metricLower = strings.ToLower(metric)
	switch metricLower {
	case "cpu_usage":
		return QueryUsageCPU
	case "cpu_request":
		return QueryRequestCPU
	case "network_transmitted":
		return QueryNetworkTransmit
	case "network_received":
		return QueryNetworkReceive
	default:
		return QueryMemoryUsageTotal
	}
}

func isMissingParamsInURL(context *gin.Context) bool {
	// Check if there is no missing parameters in the URL
	if len(context.Query("metric")) == 0 || len(context.Query("start")) == 0 || len(context.Query("end")) == 0 || len(context.Query("step")) == 0 || len(context.Query("namespace")) == 0 {
		log.Println("the parameters 'metric', 'start', 'end', 'step' and 'namespace' are mandatory")
		context.JSON(http.StatusBadRequest, "the parameters 'metric', 'start', 'end', 'step' and 'namespace' are mandatory")
		return true
	}
	return false
}

func (handler *PrometheusHandler) GetWorkflowRemoteMetric(context *gin.Context) {
	// Retrieve locally metrics from remote clusters and save them to DB
	var arrayMetricToFront []ClusterMetricToFront
	if isMissing := isMissingParamsInURL(context); isMissing {
		return
	}

	idWorkflow := context.Query("idWorkflow")
	workflowResult, _ := handler.WorkflowService.FindOne(idWorkflow)

	// Convert the parameters of the context into params to parse to the next function
	param := url.Values{}
	addParam(&param, context)

	// Retrieve and store the metrics in the DB
	arrayMetricToFront, err := StoreRemoteMetricsInDB(&workflowResult, param)
	if err != nil {
		fmt.Println("Error while storing remote metrics into DB")
	}
	context.JSON(http.StatusOK, arrayMetricToFront)
}

func StoreRemoteClusterMetricsRateToDB(workflow models.Workflow, remoteCluster string) error {
	log.Print("Retrieving metrics rates for cluster ", remoteCluster)

	// Create the URL to request metrics rates
	remoteApiPath := remoteCluster + "/api/generic-backend/cost/cluster/remote/metrics-rate"

	// Call remote API to get metrics rates
	response, err := http.Get(remoteApiPath)
	if err != nil {
		fmt.Println("Error while calling remote API to retrieve metrics rate: ", err)
	}

	metricsRate := make(map[string]float64)
	_ = json.NewDecoder(response.Body).Decode(&metricsRate)

	// If the map is empty we need to init it
	if len(workflow.RemoteMetricsRates) == 0 {
		workflow.RemoteMetricsRates = make(map[string]map[string]float64)
	}
	if len(workflow.RemoteMetricsRates[remoteCluster]) == 0 {
		log.Println("remote cluster: ", remoteCluster)
		workflow.RemoteMetricsRates[remoteCluster] = make(map[string]float64)
	}
	log.Println("remote metric rates retrieved: ", metricsRate)

	// Update workflow model with new rates
	workflow.RemoteMetricsRates[remoteCluster] = metricsRate

	// Save new workflow model to DB
	err = updateWorflowModelInDB(workflow)
	if err != nil {
		fmt.Println("Error updating workflow model in DB: ", err)
	}
	return err
}

func StoreAllRemoteClustersMetricsRatesToDB(workflow models.Workflow) error {
	// Loop over the remote clusters and store each metrics rate to DB
	remoteClustersInfo, err := extractClustersInfo(workflow)
	if err != nil {
		fmt.Println("Error extracting clusters info: ", err)
	}
	for _, cluster := range remoteClustersInfo {
		err = StoreRemoteClusterMetricsRateToDB(workflow, cluster.EndPoint)
		if err != nil {
			fmt.Println("Error storing remote cluster metrics rate to DB: ", err)
		}
	}
	return err
}

func (handler *PrometheusHandler) GetWorkflowRemoteCostAverageByMetric(context *gin.Context) {
	// Compute cost from remote workflows

	log.Println("Get cost from DB")

	// Retrieve workflow of interest
	idWorkflow := context.Query("idWorkflow")
	workflowResult, _ := handler.WorkflowService.FindOne(idWorkflow)
	metric := context.Query("metric")
	GetRemoteMetricCost(workflowResult, metric)

}

func (handler *PrometheusHandler) GetWorkflowRemoteCostAverageAllMetrics(context *gin.Context) {
	// Compute cost from remote workflows

	log.Println("Get cost from DB")

	// Retrieve workflow of interest
	idWorkflow := context.Query("idWorkflow")
	workflowResult, _ := handler.WorkflowService.FindOne(idWorkflow)
	metricList := GetMetricList()
	for _, metric := range metricList {
		GetRemoteMetricCost(workflowResult, metric)
	}

}

func GetRemoteMetricCost(workflowResult models.Workflow, metric string) {
	// Check whether we already retrieved the rates of each remote cluster
	log.Printf("########## Calculating cost for metric %s", metric)
	if len(workflowResult.RemoteMetricsRates) == 0 {
		err := StoreAllRemoteClustersMetricsRatesToDB(workflowResult)
		if err != nil {
			fmt.Println("Error storing all remote cluster metrics rate to DB: ", err)
		}
	}

	// Retrieve average metrics on pod basis for workflow of interest
	remotePodsMetricAverage := CalculateMetricsAverageFromDB(workflowResult, metric)
	remoteClustersMetricsRates := workflowResult.RemoteMetricsRates
	log.Println("metricAverage: ", remotePodsMetricAverage)
	//metricsRate := GetMetricsRate()
	for _, metricAverage := range remotePodsMetricAverage {
		log.Println("Cluster: ", metricAverage.Cluster)
		log.Println("Pod: ", metricAverage.PodName)
		log.Println("MetricAverage: ", metricAverage.MetricAverage)
		log.Println("DeltaTime: ", metricAverage.DeltaTime)
		metricCost := metricAverage.MetricAverage * (metricAverage.DeltaTime / 3600) * remoteClustersMetricsRates[metricAverage.Cluster][metric]
		log.Println("MetricCost: ", metricCost)
		log.Println("MetricHourlyPrice: ", remoteClustersMetricsRates[metricAverage.Cluster][metric])
		fmt.Println("")

	}

}

func MakeMetricsApiUrl(endpoint string, context *gin.Context) string {
	path := endpoint
	path = fmt.Sprintf(path + "idWorkflow=" + context.Query("idWorkflow"))
	path = fmt.Sprintf(path + "&start=" + context.Query("start"))
	path = fmt.Sprintf(path + "&end=" + context.Query("end"))
	path = fmt.Sprintf(path + "&step=" + context.Query("step"))
	path = fmt.Sprintf(path + "&namespace=" + context.Query("namespace"))
	path = fmt.Sprintf(path + "&metric=" + context.Query("metric"))

	return path
}

func CalculateMetricsAverageFromDB(workflowModel models.Workflow, metricName string) []MetricAverageOverTime {
	log.Println("Calculate metric average from DB values")

	prometheusResponseMetric := workflowModel.MetricResults
	podMetricAverageSlice := []MetricAverageOverTime{}

	// Loop over the different clusters and pod metrics
	for cluster, prometheusPodMetrics := range prometheusResponseMetric[metricName] {
		log.Printf("Getting metric average for each cluster for metric %s", metricName)

		// We check if the struct is not empty
		if len(prometheusPodMetrics.Data.Result) != 0 {
			podMetricAverageResponse := GetRemoteMetricAverageOverTime(cluster, prometheusPodMetrics)
			podMetricAverageSlice = append(podMetricAverageSlice, podMetricAverageResponse...)
			log.Println("podMetricAverage: ", podMetricAverageSlice)
		} else {
			log.Printf("No values found for metric %s in cluster %s", metricName, cluster)
		}

	}

	return podMetricAverageSlice
}

func StoreRemoteMetricsInDB(workflow *models.Workflow, param url.Values) ([]ClusterMetricToFront, error) {
	var arrayMetricToFront []ClusterMetricToFront
	// Retrieve worklflow model to use
	workflowResult := *workflow
	lastExecution := GetWorkflowLastExecution(workflowResult)
	log.Println("Last execution ID:", lastExecution)

	// Retrieve info of workflow remote cluster
	remoteClustersInfo, err := extractClustersInfo(workflowResult)

	if err != nil {
		fmt.Println("Error retrieving workflow last execution: ", err)
	}

	log.Print("Remote clusters found: ", remoteClustersInfo)

	arrayMetricToFront, err = updateWorkflowModelWithRemoteMetrics(remoteClustersInfo, param, &workflowResult)
	if err != nil {
		fmt.Println("Error while updating workflow model with remote metrics: ", err)
	}

	// Update the DB with new workflow model containing new metrics
	err = updateWorflowModelInDB(workflowResult)
	if err != nil {
		fmt.Println("Error while updating workflow model in DB: ", err)
	}
	return arrayMetricToFront, err
}

func updateWorkflowModelWithRemoteMetrics(clusterInfo []ClusterInfo, param url.Values, workflowResult *models.Workflow) ([]ClusterMetricToFront, error) {
	var duplicateEndPoints = make(map[string]string)
	var err error
	var arrayMetricToFront []ClusterMetricToFront
	// Retrieve cost value for each cluster
	for _, cluster := range clusterInfo {
		// The clusterMetricToFront struct is filled but not used anywhere.
		var clusterMetricToFront ClusterMetricToFront
		clusterMetricToFront.URLCluster = cluster.EndPoint
		clusterMetricToFront.LimitCluster = cluster.CostLimit
		clusterMetricToFront.DateStart = param["start"][0]
		clusterMetricToFront.DateEnd = param["end"][0]
		var step, _ = strconv.ParseFloat(param["step"][0], 32)
		clusterMetricToFront.Step = float32(step)

		// We remove duplication in cluster list
		if _, exist := duplicateEndPoints[cluster.EndPoint]; exist {
			continue
		}
		duplicateEndPoints[cluster.EndPoint] = ""
		path := fmt.Sprintf("%s/api/generic-backend/cost/cluster/?%s", cluster.EndPoint, param.Encode())

		prometheusResponseMetric, err := CallRemotePrometheusAPI(path)
		if err != nil {
			log.Printf("Error while calling remote prometheus API: %s", err)

		}

		// We add the metrics to the list corresponding to the cluster IP corresponding to metric type
		if workflowResult.MetricResults == nil {
			// We need to inialize the maps first
			workflowResult.MetricResults = make(map[string]map[string]models.MetricPrometheusResponseStruct)
		}
		if workflowResult.MetricResults[param["metric"][0]] == nil {
			workflowResult.MetricResults[param["metric"][0]] = make(map[string]models.MetricPrometheusResponseStruct)
		}
		workflowResult.MetricResults[param["metric"][0]][cluster.EndPoint] = prometheusResponseMetric
		clusterMetricToFront.PodMetric = prometheusResponseMetric
		arrayMetricToFront = append(arrayMetricToFront, clusterMetricToFront)
	}
	return arrayMetricToFront, err
}

func CallRemotePrometheusAPI(path string) (models.MetricPrometheusResponseStruct, error) {

	log.Println("path is:", path)
	response, err := http.Get(path)
	if err != nil {
		log.Printf("call remote prometheus cluster gave error: %s", err)

	}
	prometheusResponseMetric := models.MetricPrometheusResponseStruct{}
	json.NewDecoder(response.Body).Decode(&prometheusResponseMetric)
	return prometheusResponseMetric, err
}

func GetWorkflowLastExecution(workflow models.Workflow) models.Execution {
	filter := models.ExecutionFilters{WorkflowID: &workflow.Id}
	executions, _ := executions.NewHandler().Service.FindAll(filter)
	var lastExecution models.Execution

	if len(executions) > 0 {
		// sort array executions by time
		sort.SliceStable(executions, func(i, j int) bool {
			firstElement, _ := time.Parse(time.RFC3339, executions[i].StartedAt)
			secondElement, _ := time.Parse(time.RFC3339, executions[j].StartedAt)
			return firstElement.After(secondElement)
		})
		lastExecution = executions[len(executions)-1]
	}

	log.Println("Last execution ID:", lastExecution.ID)

	return lastExecution
}

func updateWorflowModelInDB(workflow models.Workflow) error {
	log.Println("Update DB with new metrics results")
	//updateDBUrl := "http://generic-backend.default.svc.cluster.local:9090/api/generic-backend/workflows/"
	updateDBUrl := "http://localhost:9090/api/generic-backend/workflows/"
	//path := fmt.Sprintf(updateDBUrl + workflow.Id.String())
	path := fmt.Sprintf(updateDBUrl + workflow.Id.Hex())

	workflowBody, err := json.Marshal(workflow)
	if err != nil {
		fmt.Println(err)
	}

	postResponse, err := http.Post(path, "models.workflow", bytes.NewBuffer(workflowBody))
	if err != nil {
		fmt.Println(err)
	}

	if postResponse.StatusCode != 200 {
		log.Println(postResponse)
	}
	return err
}

func (handler *PrometheusHandler) ReturnMetricsRateToAPICall(context *gin.Context) {
	// Retrun metrics rates to requesting generic-backend
	metricsRate := make(map[string]float64)
	metricsRate["memory_usage"] = 50
	metricsRate["cpu_usage"] = 100
	metricsRate["network_transmitted"] = 100
	metricsRate["network_received"] = 100

	context.JSON(http.StatusOK, metricsRate)
}

func GetMetricsRate() map[string]float64 {
	log.Println("Retrieving remote metric rates")
	remoteApiPath := "http://localhost:9090/api/generic-backend/cost/cluster/remote/metrics-rate"
	response, err := http.Get(remoteApiPath)
	if err != nil {
		fmt.Println("Error while calling remote API to retrieve metrics rate: ", err)
	}
	metricsRate := make(map[string]float64)
	err = json.NewDecoder(response.Body).Decode(&metricsRate)
	if err != nil {
		fmt.Println("Error decoding response body into object: ", err)
	}
	return metricsRate
}

//func GetMetricCost(metricRaw models.MetricPrometheusResponseStruct, metricType string) float64 {
//	metricAverage, deltaTime, _ := GetMetricAverageOverTime(metricRaw)
//	metricsRate := GetMetricsRate()
//	var metricRate float64
//
//	switch metricType {
//	case "cpu_usage":
//		metricRate = metricsRate.VirtualCpuPerHour
//	case "memory_usage":
//		metricRate = metricsRate.MemoryCpuPerHour
//	}
//
//	metricCost := metricAverage * (deltaTime / float64(3600)) * metricRate
//
//	return metricCost
//}

func GetRemoteMetricAverageOverTime(cluster string, metricRaw models.MetricPrometheusResponseStruct) []MetricAverageOverTime {

	timeStampInf := metricRaw.Data.Result[0].Values[0][0].(float64)
	timeStampSup := metricRaw.Data.Result[0].Values[0][0].(float64)
	podAverageMetricSlice := []MetricAverageOverTime{}
	metricValue := MetricAverageOverTime{}

	// Looping over the results of each pod
	for _, metric := range metricRaw.Data.Result {
		log.Println("Retrieving metric for pod: ", metric.Metric.Pod)
		samplesize := 0
		metriccumulatedvalue := 0.0

		if timeStampInf > metric.Values[0][0].(float64) {
			timeStampInf = metric.Values[0][0].(float64)
		}
		if timeStampSup < metric.Values[len(metric.Values)-1][0].(float64) {
			timeStampSup = metric.Values[len(metric.Values)-1][0].(float64)
		}

		// Retrieving all the values by pod for each timestamp
		for _, value := range metric.Values {
			// Convert and store metric value
			metricValue, err := strconv.ParseFloat(value[1].(string), 8)
			if err != nil {
				fmt.Println(err)
			}

			if metricValue != 0 { // We store only non-null metric values
				metriccumulatedvalue += metricValue
				samplesize++
			}
		}

		log.Println(metricValue)
		metricValue.Cluster = cluster
		metricValue.PodName = metric.Metric.Pod
		metricValue.DeltaTime = timeStampSup - timeStampInf
		metricValue.MetricAverage = metriccumulatedvalue / float64(samplesize)
		podAverageMetricSlice = append(podAverageMetricSlice, metricValue)

	}

	return podAverageMetricSlice
}

//func callClusterRemotePrometheus(context *gin.Context, path string) ([]PodMetric, error) {
//	resp, err := http.Get(path)
//	if err != nil {
//		context.Error(err)
//		context.JSON(http.StatusUnprocessableEntity, "fail to contact cluster")
//		return nil, err
//	}
//	jsonData, err := ioutil.ReadAll(resp.Body)
//
//	var data map[string]json.RawMessage
//	var arrMapsModel map[string]json.RawMessage
//	var metrics []map[string]json.RawMessage
//	var arrayMetric []ParseMetric
//	err = json.Unmarshal(jsonData, &data)
//
//	if err != nil {
//		log.Print("error read")
//		context.Error(err)
//		context.JSON(http.StatusUnprocessableEntity, "error reading parsing body response")
//		return nil, err
//	}
//	err = json.Unmarshal(data["data"], &arrMapsModel)
//
//	if err != nil {
//		log.Print("error read")
//		context.Error(err)
//		context.JSON(http.StatusUnprocessableEntity, "impossible parse metrics")
//		return nil, err
//	}
//	err = json.Unmarshal(arrMapsModel["result"], &metrics)
//	if err != nil {
//		context.Error(err)
//		context.JSON(http.StatusUnprocessableEntity, "error reading parsing body response")
//		return nil, err
//	}
//	if metrics != nil {
//		arrayMetric, err = parseMetrics(context, metrics)
//		if err != nil {
//			log.Print("error read")
//			context.Error(err)
//			context.JSON(http.StatusUnprocessableEntity, "impossible parse metrics")
//			return nil, err
//		}
//	} else {
//		return nil, nil
//	}
//
//	var result []PodMetric
//	for _, element := range arrayMetric {
//		var podMetric PodMetric
//		regx, _ := regexp.Compile("[^0-9.,]+")
//		var infometric InfoMetric
//		var nodesMetric []InfoMetric
//		var value []string
//
//		podMetric.Name = element.PodName
//		infometric.Name = context.Query("metric")
//		var elementString = string(element.MetricValues)
//		var elementReduce = elementString[1 : len(elementString)-1]
//		value = strings.Split(elementReduce, ",")
//		for index, _ := range value {
//			if index%2 == 0 {
//				valueMetric, _ := strconv.ParseFloat(regx.ReplaceAllString(value[index+1], ""), 64)
//				infometric.ValueMetric = valueMetric
//				timestamp, _ := strconv.ParseFloat(regx.ReplaceAllString(value[index], ""), 64)
//				infometric.Timestamp = timestamp
//				nodesMetric = append(nodesMetric, infometric)
//			}
//		}
//		podMetric.Metrics = nodesMetric
//		result = append(result, podMetric)
//	}
//	return result, nil
//}
//
//func parseMetrics(context *gin.Context, metrics []map[string]json.RawMessage) ([]ParseMetric, error) {
//	var arrayMetric []ParseMetric
//	for _, metric := range metrics {
//		var metricParsed ParseMetric
//		var err = json.Unmarshal(metric["metric"], &metricParsed)
//
//		if err != nil {
//			context.Error(err)
//			context.JSON(http.StatusUnprocessableEntity, "error parsing with model response")
//			return nil, err
//		}
//		err = json.Unmarshal(metric["values"], &metricParsed.MetricValues)
//		if err != nil {
//			context.Error(err)
//			context.JSON(http.StatusUnprocessableEntity, "error parsing with model response")
//			return nil, err
//		}
//		arrayMetric = append(arrayMetric, metricParsed)
//	}
//	return arrayMetric, nil
//}

/*func filtrage(nodecost []InfoMetric, s string) []InfoMetric {
	var nodecostFiltre []InfoMetric
	for _, element := range nodecost {
		if len(element.MetricValue.Pod) > 0 && strings.Contains(element.MetricValue.Pod, s) {
			nodecostFiltre = append(nodecostFiltre, element)
		}
	}
	return nodecostFiltre
}*/

func addParam(param *url.Values, context *gin.Context) {
	var filters = [...]string{"metric", "namespace", "cluster", "start", "end", "container", "step"}
	for _, filter := range filters {
		valueFilter := context.Query(filter)
		if len(valueFilter) > 0 {
			param.Add(filter, strings.ToLower(valueFilter))
		}
	}
}

type PrometheusHandler struct {
	WorkflowService *workflow.Service
}

/*func isCostLimitBlock(result *[]ResultKubecost, execution models.Execution) {
	if len(execution.Tasks) > 0 {
		log.Print("execution not nil")
		var limitTask = make(map[string]float32)
		for _, task := range execution.Tasks {
			limitTask[task.PodName] = task.CostLimit
		}
		for _, clusterCost := range *result {
			var arrayCost = clusterCost.NodeCost
			for _, podCost := range arrayCost {
				if limitTask[extractNamePod(podCost.Environment)] > 0 {
					podCost.BlockCostLimitExceeded = limitTask[extractNamePod(podCost.Environment)] < podCost.TotalCost
				}
			}
		}
	}
}*/

func extractClustersInfo(result models.Workflow) ([]ClusterInfo, error) {
	pat := regexp.MustCompile("(https?://[^/]+)")
	var duplicateEndPoints = make(map[string]string)
	var clusterInfoList []ClusterInfo
	for _, block := range result.Blocks {
		var clusterInfo ClusterInfo
		endpoint := block.Payload.ComputeProvider.EndpointLocation
		clusterInfo.EndPoint = pat.FindStringSubmatch(endpoint)[1]
		clusterInfo.CostLimit = block.Payload.ComputeProvider.CostLimitCluster
		if _, exist := duplicateEndPoints[endpoint]; exist {
			continue
		}
		duplicateEndPoints[endpoint] = ""
		clusterInfoList = append(clusterInfoList, clusterInfo)
	}
	return clusterInfoList, nil
}

type CostHandler struct {
	WorkflowService *workflow.Service
}
