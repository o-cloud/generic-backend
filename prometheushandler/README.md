# Prometheus API
The Prometheus API is in charge of retrieving the metrics of the different running pods. 
The Prometheus API of several clusters can communicate with each other in order. This communcation allows
the Prometheus API of one cluster to retrieve the metrics of a running worflow on a remote cluster.

## API endpoints
**NOTE**: all the parameters are passed as parameters in the URL.
| Method | Endpoint | Function | Action |
| ------ | -------- | -------- | ------ |
| GET | `/cluster` 				| GetLocalClusterMetrics | Retrieve all local metrics |
| GET | `/cluster/remote/metrics` 		| GetWorkflowRemoteMetric | Ask generic-backend to retrieve remote metrics |
| GET | `/cluster/remote/cost-by-metric` 	| GetWorkflowRemoteCostAverageByMetric | Compute cost of workflow on remote cluster by metrics |
| GET | `/cluster/remote/cost-all-metrics` 	| GetWorkflowRemoteCostAverageAllMetrics | Compute cost of workflow on remove cluster for all metrics |
| GET | `/cluster/remote/metrics-rate` 		| ReturnMetricsRateToAPICall | Return fake rates model (Should not be used anymore since we have pricing-manager) |

## Worfklow metrics
When the prometheus API requests a workflow metrics from remote cluster, the metrics retrieved are added to the workflow model and then saved to 
the database. All the information required for the calculation of each stage of the workflow is stored in the workflow model.

## Prometheus API layout
![prometheus API](prometheusAPILogic.drawio.svg)
