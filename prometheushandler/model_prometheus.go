package prometheushandler

import (
	"encoding/json"

	"gitlab.com/o-cloud/generic-backend/models"
)

type InfoMetric struct {
	Name        string  `form:"name" json:"name"`
	Timestamp   float64 `form:"timestamp" json:"timestamp"`
	ValueMetric float64 `form:"value" json:"value"`
}
type ClusterMetricToFront struct {
	URLCluster    string                                `form:"urlcluster" json:"urlcluster"`
	DateStart     string                                `form:"datestart" json:"datestart"`
	DateEnd       string                                `form:"datend" json:"datend"`
	Step          float32                               `form:"step" json:"step"`
	LimitCluster  float32                               `form:"limitcluster" json:"limitcluster"`
	PodMetric     models.MetricPrometheusResponseStruct `form:"pod" json:"pod"`
	IsLimitExceed bool                                  `form:"islimitexceed" json:"islimitexceed"`
}
type PodMetric struct {
	Name          string       `form:"namepod" json:"namepod"`
	CoutMoyen     float32      `form:"coutmoyen" json:"coutmoyen"`
	CoutTotalPod  float32      `form:"coutotalpod" json:"coutotalpod"`
	LimitPod      float32      `form:"limitpod" json:"limitpod"`
	IsLimitExceed bool         `form:"islimitexceed" json:"islimitexceed"`
	Metrics       []InfoMetric `form:"metrics" json:"metrics"`
}

type ClusterInfo struct {
	EndPoint  string
	CostLimit float32
}

type ParseMetric struct {
	PodName      string `form:"pod" json:"pod"`
	MetricValues json.RawMessage
}

type MetricAverageOverTime struct {
	Cluster       string  `form:"cluster" json:"cluster"`
	PodName       string  `form:"podName" json:"podName"`
	MetricAverage float64 `form:"metricAverage" json:"metricAverage"`
	DeltaTime     float64 `form:"deltaTime" json:"deltaTime"`
}

//type MetricPrometheusResponseStruct struct {
//	Status string `json:"status"`
//	Data   struct {
//		ResultType string `json:"resultType"`
//		Result     []struct {
//			Metric struct {
//				Pod string `json:"pod"`
//			} `json:"metric"`
//			Values [][]interface{} `json:"values"`
//		} `json:"result"`
//	} `json:"data"`
//}

type MetricRatesStruct struct {
	VirtualCpuPerHour float64 `json:virtualCpuPerHour`
	MemoryCpuPerHour  float64 `json:memoryCpuPerHour`
}
