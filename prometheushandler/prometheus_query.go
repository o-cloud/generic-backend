package prometheushandler

var QueryNamespace = "%s/api/v1/query?query=%s"
var QueryRange = "%s/api/v1/query_range?query=%s"
var QueryUsageCPU = "sum(container_cpu_usage_seconds_total{cluster=\"%s\",namespace=\"%s\"})by(pod)"

var QueryRequestCPU = "sum(cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests{cluster=\"%s\",namespace=\"%s\"})by(pod)"

var QueryMemoryUsageTotal = "sum(container_memory_working_set_bytes{cluster=\"%s\",namespace=\"%s\"})by(pod)"

var QueryNetworkTransmit = "sum(container_network_transmit_bytes_total{cluster=\"%s\",namespace=\"%s\"})by(pod)"

var QueryNetworkReceive = "sum(container_network_receive_bytes_total{cluster=\"%s\",namespace=\"%s\"})by(pod)"

var QueryByTime = QueryRange + "&%s"
