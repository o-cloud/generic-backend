package executions

import (
	"bytes"
	"context"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	argoClient "github.com/argoproj/argo-workflows/v3/cmd/argo/commands/client"
	"github.com/argoproj/argo-workflows/v3/pkg/apiclient/workflow"
	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/o-cloud/generic-backend/models"
	v12 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func getPodLogs(podName string, namespace string, client *kubernetes.Clientset) map[string][]string {
	res := make(map[string][]string, 0)
	containers := []string{"main", "wait"}
	for _, container := range containers {
		req := client.CoreV1().Pods(namespace).GetLogs(podName, &v12.PodLogOptions{
			Container: container,
		})
		podLogs, err := req.Stream(context.Background())
		if err != nil {
			log.Println(err)
			continue
		}
		buf := new(bytes.Buffer)
		_, err = io.Copy(buf, podLogs)
		if err != nil {
			log.Println(err)
			continue
		}
		res[container] = strings.Split(buf.String(), "\n")
	}
	return res
}

func monitor(execution models.Execution, repo Repository) {
	ctx, client := argoClient.NewAPIClient()
	wfService := client.NewWorkflowServiceClient()
	var status *v1alpha1.WorkflowPhase
	timeout := 0
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Fatalf("cannot create kube config, %s", err)
	}
	kubernetesClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalf("cannot create kube client, %s", err)
	}
	for status == nil || !status.Completed() {
		//Reset exec counter
		log.Println("monitoring workflow " + execution.ArgoName)
		pods, err := kubernetesClient.CoreV1().Pods(execution.Namespace).List(context.Background(), v1.ListOptions{
			LabelSelector: "workflows.argoproj.io/workflow=" + execution.ArgoName,
		})
		if err != nil {
			log.Println("error listing pods in wf", err)
		}
		for _, item := range pods.Items {
			indexStr, found := item.Annotations["blockIndex"]
			if !found {
				log.Printf("Cannot find blockIndex in pod %s annotations", item.Name)
				continue
			}
			blockIndex, err := strconv.Atoi(indexStr)
			if err != nil {
				log.Printf("cannot convert block index, %s\n", err)
				continue
			}
			execution.Tasks[blockIndex].PodName = item.Name
			execution.Tasks[blockIndex].Logs = getPodLogs(item.Name, execution.Namespace, kubernetesClient)
		}

		wf, err := wfService.GetWorkflow(ctx, &workflow.WorkflowGetRequest{
			Name:      execution.ArgoName,
			Namespace: execution.Namespace,
		})
		if err != nil {
			log.Printf("error in execution monitor, %s\n", err)
			if timeout > 300 {
				break
			} else {
				timeout += 5
				time.Sleep(5 * time.Second)
				continue
			}
		}
		if wf.Status.Phase == v1alpha1.WorkflowUnknown {
			execution.Status = "Created"
		} else {
			execution.Status = string(wf.Status.Phase)
		}
		if !wf.Status.StartedAt.IsZero() {
			execution.StartedAt = wf.Status.StartedAt.Time.Format(time.RFC3339)
		}
		if !wf.Status.FinishedAt.IsZero() {
			execution.FinishedAt = wf.Status.FinishedAt.Time.Format(time.RFC3339)
		}
		for _, nodeStatus := range wf.Status.Nodes {

			template, found := findTemplateWithName(nodeStatus.TemplateName, wf.Spec.Templates)
			if !found {
				log.Printf("cannot find template with name %s\n", nodeStatus.TemplateName)
				continue
			}
			if template.DAG != nil {
				//No need to follow dag template, we continue
				continue
			}
			indexStr, found := template.Metadata.Annotations["blockIndex"]
			if !found {
				log.Printf("cannot find block index in template's %s annotations\n", nodeStatus.TemplateName)
				continue
			}
			blockIndex, err := strconv.Atoi(indexStr)
			if err != nil {
				log.Printf("cannot convert block index, %s\n", err)
				continue
			}
			execution.Tasks[blockIndex].Status = string(nodeStatus.Phase)
			if execution.Tasks[blockIndex].NodeStatus == nil {
				execution.Tasks[blockIndex].NodeStatus = make(map[string]string)
			}
			execution.Tasks[blockIndex].NodeStatus[nodeStatus.ID] = string(nodeStatus.Phase)
			if !nodeStatus.StartedAt.IsZero() {
				execution.Tasks[blockIndex].StartedAt = nodeStatus.StartedAt.Time.Format(time.RFC3339)
			}
			if !nodeStatus.FinishedAt.IsZero() {
				execution.Tasks[blockIndex].FinishedAt = nodeStatus.FinishedAt.Time.Format(time.RFC3339)
			}
		}
		status = &wf.Status.Phase
		err = repo.UpdateOne(execution.ID.Hex(), execution)
		if err != nil {
			log.Printf("error while updating execution, %s\n", err)
		}
		log.Printf("workflow status %s %s\n", wf.Status.Phase, wf.Status.Progress)
		log.Printf("templates len %d status.node len %d\n", len(wf.Spec.Templates), len(wf.Status.Nodes))
		time.Sleep(5 * time.Second)
	}
}

func findTemplateWithName(name string, templates []v1alpha1.Template) (*v1alpha1.Template, bool) {
	for _, template := range templates {
		if template.Name == name {
			return &template, true
		}
	}
	return nil, false
}
