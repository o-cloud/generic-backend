package executions

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/generic-backend/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

type Handler struct {
	Service Service
}

func NewHandler() *Handler {
	return &Handler{Service: NewService()}
}

func (h *Handler) SetUpRoutes(router *gin.RouterGroup) {
	router.GET("", h.getAllExecutionsHandler)
	router.GET("/:id", h.getOneExecutionHandler)
	router.GET("/statusCount", h.getStatusCount)
}

func (h *Handler) getAllExecutionsHandler(c *gin.Context) {
	filters, err := parseFilters(c)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusUnprocessableEntity)
		return
	}
	res, err := h.Service.FindAll(*filters)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, res)
}

func parseFilters(c *gin.Context) (*models.ExecutionFilters, error) {
	filters := models.ExecutionFilters{}
	workflowID, exists := c.GetQuery("workflow")
	if exists {
		id, err := primitive.ObjectIDFromHex(workflowID)
		if err != nil {
			return nil, err
		}
		filters.WorkflowID = &id
	}
	startTime, exists := c.GetQuery("startTime")
	if exists {
		t, err := time.Parse(time.RFC3339, startTime)
		if err != nil {
			return nil, err
		}
		filters.StartTime = &t
	}
	return &filters, nil
}

func (h *Handler) getOneExecutionHandler(c *gin.Context) {
	id := c.Param("id")
	r, err := h.Service.FindOne(id)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusNotFound)
		return
	}
	c.JSON(http.StatusOK, r)
}

func (h *Handler) getStatusCount(c *gin.Context) {
	filters, err := parseFilters(c)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusUnprocessableEntity)
		return
	}
	res, err := h.Service.CountStatus(*filters)
	if err != nil {
		_ = c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, res)
}
