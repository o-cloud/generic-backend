package executions

import (
	"context"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/o-cloud/generic-backend/models"
	mongo_helper "gitlab.com/o-cloud/generic-backend/mongo-helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type Repository interface {
	Insert(execution models.Execution) (*models.Execution, error)
	FindAll(filters models.ExecutionFilters) ([]models.Execution, error)
	FindOne(id string) (models.Execution, error)
	UpdateOne(id string, execution models.Execution) error
	CountStatus(filters models.ExecutionFilters) (*models.ExecutionStatusCount, error)
}

type repositoryImpl struct {
	col *mongo_helper.Collection
}

func NewRepository() Repository {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))
	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}
	c := mongo_helper.NewCollection(client.Database("genericBackend").Collection("executions"))

	return &repositoryImpl{
		col: c,
	}
}

type ExecutionDbRecord struct {
	ID           primitive.ObjectID  `bson:"_id,omitempty"`
	Namespace    string              `bson:"namespace"`
	ArgoName     string              `bson:"argoName"`
	WorkflowName string              `bson:"workflowName"`
	Status       string              `bson:"status"`
	WorkflowID   primitive.ObjectID  `bson:"workflowId"`
	StartedAt    *primitive.DateTime `bson:"startedAt"`
	FinishedAt   *primitive.DateTime `bson:"finishedAt"`
	Tasks        []models.Task       `bson:"tasks"`
}

func dbRecordToExecution(record ExecutionDbRecord) *models.Execution {
	startedAt := ""
	if record.StartedAt != nil {
		startedAt = record.StartedAt.Time().Format(time.RFC3339)
	}
	finishedAt := ""
	if record.FinishedAt != nil {
		finishedAt = record.FinishedAt.Time().Format(time.RFC3339)
	}
	return &models.Execution{
		ID:           record.ID,
		Namespace:    record.Namespace,
		ArgoName:     record.ArgoName,
		WorkflowName: record.WorkflowName,
		Status:       record.Status,
		WorkflowID:   record.WorkflowID,
		StartedAt:    startedAt,
		FinishedAt:   finishedAt,
		Tasks:        record.Tasks,
	}
}

func executionToDbRecord(execution models.Execution) (*ExecutionDbRecord, error) {
	var startDbTime *primitive.DateTime
	var finishedDbTime *primitive.DateTime

	startTime, err := time.Parse(time.RFC3339, execution.StartedAt)
	if err != nil {
		log.Printf("cannot convert start time %s to RFC3339", startTime)
	} else {
		tmp := primitive.NewDateTimeFromTime(startTime)
		startDbTime = &tmp
	}
	finishedTime, err := time.Parse(time.RFC3339, execution.FinishedAt)
	if err != nil {
		log.Printf("cannot convert finished time %s to RFC3339", finishedTime)
	} else {
		tmp := primitive.NewDateTimeFromTime(finishedTime)
		finishedDbTime = &tmp
	}
	return &ExecutionDbRecord{
		ID:           execution.ID,
		Namespace:    execution.Namespace,
		ArgoName:     execution.ArgoName,
		WorkflowName: execution.WorkflowName,
		Status:       execution.Status,
		WorkflowID:   execution.WorkflowID,
		StartedAt:    startDbTime,
		FinishedAt:   finishedDbTime,
		Tasks:        execution.Tasks,
	}, nil
}

func recordSliceToExecution(records []ExecutionDbRecord) []models.Execution {
	res := make([]models.Execution, len(records))
	for i := range records {
		res[i] = *dbRecordToExecution(records[i])
	}
	return res
}

func (r *repositoryImpl) Insert(execution models.Execution) (*models.Execution, error) {
	record, err := executionToDbRecord(execution)
	if err != nil {
		return nil, fmt.Errorf("cannot convert execution to db record, %s", err)
	}
	res, errInsert := r.col.InsertOne(record)
	if errInsert != nil {
		log.Fatal(errInsert)
	}
	execution.ID = res
	return &execution, errInsert
}

func (r *repositoryImpl) FindAll(filters models.ExecutionFilters) ([]models.Execution, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	f := bson.M{}
	if filters.WorkflowID != nil {
		f["workflowId"] = *filters.WorkflowID
	}
	records := make([]ExecutionDbRecord, 0)
	c, err := r.col.Col.Find(ctx, f)
	if err != nil {
		return make([]models.Execution, 0), err
	}
	defer c.Close(ctx)
	err = c.All(ctx, &records)
	return recordSliceToExecution(records), err
}

func (r *repositoryImpl) UpdateOne(id string, execution models.Execution) error {
	record, err := executionToDbRecord(execution)
	if err != nil {
		return fmt.Errorf("cannot convert execution to db record, %s", err)
	}
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = r.col.Col.ReplaceOne(ctx, bson.M{"_id": objectID}, record)
	return err
}

func (r *repositoryImpl) FindOne(id string) (models.Execution, error) {
	var record ExecutionDbRecord
	err := r.col.FindOneById(id, &record)
	if err == mongo.ErrNoDocuments {
		fmt.Println("record does not exist")
	}
	return *dbRecordToExecution(record), err
}

func (r *repositoryImpl) CountStatus(filters models.ExecutionFilters) (*models.ExecutionStatusCount, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	runningStatus := bson.E{Key: "$or", Value: bson.A{
		bson.M{"status": "Running"},
		bson.M{"status": "Pending"},
		bson.M{"status": "Created"},
	}}
	failedStatus := bson.E{Key: "$or", Value: bson.A{
		bson.M{"status": "Failed"},
		bson.M{"status": "Error"},
	}}
	succeededStatus := bson.E{Key: "status", Value: "Succeeded"}
	tempFilter := bson.M{}

	if filters.WorkflowID != nil {
		tempFilter["workflowId"] = *filters.WorkflowID
	}
	if filters.StartTime != nil {
		tempFilter["startedAt"] = bson.M{"$gte": primitive.NewDateTimeFromTime(*filters.StartTime)}
	}
	f := bson.E{Key: "$and", Value: bson.A{tempFilter}}

	running, err := r.col.Col.CountDocuments(ctx, bson.D{runningStatus, f})
	if err != nil {
		return nil, fmt.Errorf("error while counting running executions, %s", err)
	}
	failed, err := r.col.Col.CountDocuments(ctx, bson.D{failedStatus, f})
	if err != nil {
		return nil, fmt.Errorf("error while counting failed executions, %s", err)
	}
	succeeded, err := r.col.Col.CountDocuments(ctx, bson.D{succeededStatus, f})
	if err != nil {
		return nil, fmt.Errorf("error while counting succeeded executions, %s", err)
	}

	return &models.ExecutionStatusCount{
		Succeeded: succeeded,
		Failed:    failed,
		Running:   running,
	}, nil
}
