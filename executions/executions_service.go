package executions

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/o-cloud/generic-backend/config"
	"gitlab.com/o-cloud/generic-backend/models"
)

type Service interface {
	ExecuteWorkflow(workflow models.Workflow) (*models.Execution, error)
	FindAll(filters models.ExecutionFilters) ([]models.Execution, error)
	FindOne(id string) (models.Execution, error)
	CountStatus(filters models.ExecutionFilters) (*models.ExecutionStatusCount, error)
}

func NewService() Service {
	return &serviceImpl{
		repository: NewRepository(),
	}
}

type serviceImpl struct {
	repository Repository
}

type executeWorkflowResponse struct {
	Namespace string            `json:"namespace"`
	Workflow  v1alpha1.Workflow `json:"workflow"`
}

func (s *serviceImpl) ExecuteWorkflow(workflow models.Workflow) (*models.Execution, error) {
	err := workflow.SortBlocks()
	workflow.ReindexBlocks()
	if err != nil {
		return nil, err
	}
	jsonData, err := json.Marshal(workflow)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal workflow, %s", err)
	}
	request, err := http.Post(config.Config.PipelineOrchestratorStartWfParallelURI, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, fmt.Errorf("error while requesting pipeline orchestrator, %s", err)
	}
	var result executeWorkflowResponse
	err = json.NewDecoder(request.Body).Decode(&result)
	if err != nil {
		return nil, fmt.Errorf("error while reading response from pipeline orchestrator, %s", err)
	}
	execution := &models.Execution{
		Namespace:    result.Namespace,
		ArgoName:     result.Workflow.Name,
		WorkflowName: workflow.Name,
		Status:       "Created",
		WorkflowID:   workflow.Id,
		Tasks:        make([]models.Task, 0),
	}
	for _, block := range workflow.Blocks {
		execution.Tasks = append(execution.Tasks, models.Task{
			Name:            block.Name,
			BlockID:         block.Index,
			Status:          "Not Started",
			Logs:            make(map[string][]string, 0),
			ComputeProvider: block.Payload.ComputeProvider,
			CostLimit:       block.Payload.CostLimitBlock,
		})
	}
	execution, err = s.repository.Insert(*execution)
	if err != nil {
		return nil, fmt.Errorf("error while saving execution, %s", err)
	}
	go monitor(*execution, s.repository)
	return execution, nil
}

func (s *serviceImpl) FindAll(filters models.ExecutionFilters) ([]models.Execution, error) {
	return s.repository.FindAll(filters)
}

func (s *serviceImpl) FindOne(id string) (models.Execution, error) {
	return s.repository.FindOne(id)
}

func (s *serviceImpl) CountStatus(filters models.ExecutionFilters) (*models.ExecutionStatusCount, error) {
	return s.repository.CountStatus(filters)
}
