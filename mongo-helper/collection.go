package mongo_helper

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type Collection struct {
	Col *mongo.Collection
}

func NewCollection(c *mongo.Collection) *Collection {
	return &Collection{Col: c}
}

func (c *Collection) InsertOne(document interface{}) (primitive.ObjectID, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := c.Col.InsertOne(ctx, document)
	if err != nil {
		return [12]byte{}, err
	}
	return res.InsertedID.(primitive.ObjectID), nil
}

func (c *Collection) FindAll(results interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cur, err := c.Col.Find(ctx, bson.D{})
	if err != nil {
		log.Fatal(err)
		return err
	}
	defer cur.Close(ctx)
	cur.All(ctx, results)
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	return err
}

func (c *Collection) FindOneById(id string, result interface{}) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = c.Col.FindOne(ctx, bson.M{"_id": objectID}).Decode(result)
	return err
}

func (c *Collection) DeleteOneById(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = c.Col.DeleteOne(ctx, bson.M{"_id": objectID})
	return err
}
