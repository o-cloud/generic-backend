module gitlab.com/o-cloud/generic-backend

go 1.16

require (
	github.com/argoproj/argo-workflows/v3 v3.2.0-rc2
	github.com/gin-gonic/gin v1.7.2
	github.com/gorilla/websocket v1.4.2
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/catalog v0.0.0-20211026092148-685ec4df5feb
	gitlab.com/o-cloud/cluster-discovery-api v0.0.0-20211021122147-299fd520c0b0
	gitlab.com/o-cloud/cwl v0.0.0-20211021091742-30f22b2b3879
	go.mongodb.org/mongo-driver v1.5.2
	k8s.io/api v0.21.1
	k8s.io/apimachinery v0.21.1
	k8s.io/client-go v0.21.1
)
