# Builder Image
FROM golang:1.16 AS builder

RUN echo "machine gitlab.com login docker-build password wyktXwsDzJqZQkKUfgqk" > ~/.netrc

WORKDIR /usr/src

ENV GOPRIVATE=gitlab.com/o-cloud/*

COPY ./go.mod ./go.sum ./

RUN go mod download 

COPY . .

# Pass skaffold debug when used (by default is empty)
ARG SKAFFOLD_GO_GCFLAGS

RUN CGO_ENABLED=0 go build -gcflags="${SKAFFOLD_GO_GCFLAGS}" -o /go/bin/gin-test

# Final image
FROM scratch
COPY ./config.yaml /etc/irtsb/
COPY --from=builder /go/bin/gin-test /go/bin/gin-test

ENV GOTRACEBACK=all

ENTRYPOINT ["/go/bin/gin-test"]

