package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/generic-backend/catalog"
	"gitlab.com/o-cloud/generic-backend/config"
	"gitlab.com/o-cloud/generic-backend/executions"
	"gitlab.com/o-cloud/generic-backend/favorites"
	"gitlab.com/o-cloud/generic-backend/prometheushandler"
	"gitlab.com/o-cloud/generic-backend/providers"
	"gitlab.com/o-cloud/generic-backend/workflow"
	"log"
)

func main() {
	config.Load()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(CORSMiddleware())
	favorites.NewHandler().SetupRoutes(r.Group("/api/generic-backend/favorites"))
	providers.NewHandler().SetupRoutes(r.Group("/api/generic-backend/providers"))
	catalog.NewHandler().SetUpRoutes(r.Group("/api/generic-backend/catalog"))
	workflow.NewHandler().SetUpRoutes(r.Group("/api/generic-backend/workflows"))
	executions.NewHandler().SetUpRoutes(r.Group("/api/generic-backend/executions"))
	handlerCost := prometheushandler.PrometheusHandler{WorkflowService: workflow.GetWorkflowService()}
	handlerCost.PrometheusAPI(r.Group("/api/generic-backend/cost"))
	return r
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
