package config

import (
	"log"

	"github.com/spf13/viper"
)

var (
	Config ServiceConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	//Environment variables
	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	viper.BindEnv("MONGO_CON_STRING")

	viper.SetDefault("PRICING_MANAGER_API", "http://pricing-manager:9080")
	viper.BindEnv("PRICING_MANAGER_API")
}

type ServiceConfig struct {
	MongoDbUri                             string
	CatalogUri                             string
	PrometheusUri                          string
	PublicCatalogPrefix                    string
	Server                                 ServerServiceConfig
	PeersDiscoveryUri                      string
	PipelineOrchestratorStartWfParallelURI string
}

type ServerServiceConfig struct {
	ListenAddress string
}
